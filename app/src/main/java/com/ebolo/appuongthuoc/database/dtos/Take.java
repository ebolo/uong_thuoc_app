package com.ebolo.appuongthuoc.database.dtos;

import com.ebolo.appuongthuoc.database.entities.Drug;

import java.util.ArrayList;
import java.util.List;

public class Take {
    private int hour, minute;
    private List<Drug> drugs = new ArrayList<>();

    public Take(int hour, int minute, List<Drug> drugs) {
        this.hour = hour;
        this.minute = minute;
        this.drugs = drugs;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public List<Drug> getDrugs() {
        return drugs;
    }

    public void setDrugs(List<Drug> drugs) {
        this.drugs = drugs;
    }

    public void addDrug(final Drug drug) {
        drugs.add(drug);
    }
}
