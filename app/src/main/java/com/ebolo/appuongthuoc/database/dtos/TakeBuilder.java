package com.ebolo.appuongthuoc.database.dtos;

import com.ebolo.appuongthuoc.database.entities.Drug;

import java.util.ArrayList;
import java.util.List;

public class TakeBuilder {
    private int hour;
    private int minute;
    private List<Drug> drugs;

    public TakeBuilder setHour(int hour) {
        this.hour = hour;
        return this;
    }

    public TakeBuilder setMinute(int minute) {
        this.minute = minute;
        return this;
    }

    public TakeBuilder setDrugs(List<Drug> drugs) {
        this.drugs = drugs;
        return this;
    }

    public TakeBuilder addDrug(Drug drug) {
        if (this.drugs == null)
            this.drugs = new ArrayList<>();
        this.drugs.add(drug);
        return this;
    }

    public Take createTake() {
        if (this.drugs == null)
            this.drugs = new ArrayList<>();
        return new Take(hour, minute, drugs);
    }
}