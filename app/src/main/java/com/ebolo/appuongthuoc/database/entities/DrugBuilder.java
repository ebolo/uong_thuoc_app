package com.ebolo.appuongthuoc.database.entities;

import java.io.Serializable;
import java.util.List;

public class DrugBuilder implements Serializable {
    private String name;
    private List<Reminder> reminders;
    private Instruction instruction;
    private Schedule schedule;

    public DrugBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public DrugBuilder setReminders(List<Reminder> reminders) {
        this.reminders = reminders;
        return this;
    }

    public DrugBuilder setInstruction(Instruction instruction) {
        this.instruction = instruction;
        return this;
    }

    public DrugBuilder setSchedule(Schedule schedule) {
        this.schedule = schedule;
        return this;
    }

    public Drug createDrug() {
        return new Drug(name, reminders, instruction, schedule);
    }
}