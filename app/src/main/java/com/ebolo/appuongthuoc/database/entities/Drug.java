package com.ebolo.appuongthuoc.database.entities;

import java.io.Serializable;
import java.util.List;

public class Drug implements Serializable {
    private String name;
    private List<Reminder> reminders;
    private Instruction instruction;
    private Schedule schedule;

    public Drug(String name, List<Reminder> reminders, Instruction instruction, Schedule schedule) {
        this.name = name;
        this.reminders = reminders;
        this.instruction = instruction;
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Reminder> getReminders() {
        return reminders;
    }

    public void setReminders(List<Reminder> reminders) {
        this.reminders = reminders;
    }

    public Instruction getInstruction() {
        return instruction;
    }

    public void setInstruction(Instruction instruction) {
        this.instruction = instruction;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }
}
