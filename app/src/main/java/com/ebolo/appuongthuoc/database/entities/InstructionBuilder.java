package com.ebolo.appuongthuoc.database.entities;

public class InstructionBuilder {
    private String freeInstruction;
    private int foodInstruction;

    public InstructionBuilder setFreeInstruction(String freeInstruction) {
        this.freeInstruction = freeInstruction;
        return this;
    }

    public InstructionBuilder setFoodInstruction(int foodInstruction) {
        this.foodInstruction = foodInstruction;
        return this;
    }

    public Instruction createInstruction() {
        return new Instruction(freeInstruction, foodInstruction);
    }
}