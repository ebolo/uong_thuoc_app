package com.ebolo.appuongthuoc.database.entities;

import java.io.Serializable;

public class Reminder implements Serializable {
    private int hour, minute;
    private float quantity = 1f;

    public Reminder(int hour, int minute, float quantity) {
        this.hour = hour;
        this.minute = minute;
        this.quantity = quantity;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }
}
