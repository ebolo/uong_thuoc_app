package com.ebolo.appuongthuoc.database.entities;

import android.support.annotation.IntDef;

import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Instruction implements Serializable {
    public static final int BEFORE = 0, WITH = 1, AFTER = 2;

    private String freeInstruction;
    private int foodInstruction;

    @IntDef({BEFORE, WITH, AFTER})
    @Retention(RetentionPolicy.SOURCE)
    public @interface FoodInstruction{}

    public Instruction(String freeInstruction, @FoodInstruction int foodInstruction) {
        this.freeInstruction = freeInstruction;
        this.foodInstruction = foodInstruction;
    }

    public Instruction() {
        freeInstruction = "";
        foodInstruction = BEFORE;
    }

    public String getFreeInstruction() {
        return freeInstruction;
    }

    public void setFreeInstruction(String freeInstruction) {
        this.freeInstruction = freeInstruction;
    }

    public int getFoodInstruction() {
        return foodInstruction;
    }

    public void setFoodInstruction(@FoodInstruction int foodInstruction) {
        this.foodInstruction = foodInstruction;
    }
}
