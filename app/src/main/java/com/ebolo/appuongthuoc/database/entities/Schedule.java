package com.ebolo.appuongthuoc.database.entities;

import android.support.annotation.IntDef;

import org.threeten.bp.Instant;

import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Schedule implements Serializable {
    public static final int
            MONDAY = 1,
            TUESDAY = 2,
            WEDNESDAY = 3,
            THURSDAY = 4,
            FRIDAY = 5,
            SATURDAY = 6,
            SUNDAY = 7;

    private final boolean[] dates = new boolean[7];
    private Instant startDate = Instant.now();

    @IntDef({
            MONDAY,
            TUESDAY,
            WEDNESDAY,
            THURSDAY,
            FRIDAY,
            SATURDAY,
            SUNDAY
    })
    @Retention(RetentionPolicy.SOURCE)
    public @interface ScheduleDay {}

    public void setDate(@ScheduleDay int dayOfWeek, boolean value) {
        dates[dayOfWeek - 1] = value;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public boolean isAlarmingFor(@ScheduleDay int dayOfWeek) {
        return dates[dayOfWeek - 1];
    }
}
