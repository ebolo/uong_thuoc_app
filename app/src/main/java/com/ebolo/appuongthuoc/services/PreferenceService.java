package com.ebolo.appuongthuoc.services;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceService {
    public static SharedPreferences getPreference(
            final Activity activity,
            final String prefFileKey
    ) {
        return activity.getSharedPreferences(prefFileKey, Context.MODE_PRIVATE);
    }
}
