package com.ebolo.appuongthuoc.services;

import android.app.DatePickerDialog;
import android.support.v4.app.FragmentManager;

import com.ebolo.appuongthuoc.fragments.DatePickerFragment;

import org.threeten.bp.LocalDateTime;

public class UiService {
    public static void showDatePicker(
            final LocalDateTime initialDate,
            final DatePickerDialog.OnDateSetListener listener,
            final FragmentManager manager
    ) {
        DatePickerFragment fragment = DatePickerFragment.newInstance(listener, initialDate);
        fragment.show(manager, "datePicker");
    }
}
