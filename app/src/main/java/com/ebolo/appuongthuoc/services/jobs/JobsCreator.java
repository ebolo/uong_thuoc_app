package com.ebolo.appuongthuoc.services.jobs;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

public class JobsCreator implements JobCreator {
    @Nullable
    @Override
    public Job create(@NonNull String tag) {
        switch (tag) {
            case AlarmJob.TAG:
                return new AlarmJob();
            case AlarmUpdateJob.TAG:
                return new AlarmUpdateJob();
            default:
                return null;
        }
    }
}
