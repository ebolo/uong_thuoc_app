package com.ebolo.appuongthuoc.services.jobs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.Function;
import com.ebolo.appuongthuoc.R;
import com.ebolo.appuongthuoc.database.dtos.Take;
import com.ebolo.appuongthuoc.database.entities.Drug;
import com.ebolo.appuongthuoc.services.DatabaseService;
import com.evernote.android.job.DailyJob;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;
import com.evernote.android.job.util.support.PersistableBundleCompat;

import org.threeten.bp.DayOfWeek;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneOffset;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.paperdb.Paper;

public class AlarmUpdateJob extends DailyJob {
    static final String TAG = "alarm_create_job";

    @NonNull
    @Override
    protected DailyJobResult onRunDailyJob(Params params) {
        try {
            Paper.init(getContext());
            final String lastUserId = getContext().getSharedPreferences(
                    getContext().getString(R.string.general_pref_file_key),
                    Context.MODE_PRIVATE
            ).getString("lastUserId", null);
            reSchedule(lastUserId);
            Log.e(TAG,"Scheduled");
            return DailyJobResult.SUCCESS;
        } catch (final Exception exception) {
            Log.e(TAG,"Canceled");
            return DailyJobResult.CANCEL;
        }
    }

    /**
     * This should only be called within app session
     * @param lastUserId user id
     */
    public static void reSchedule(final String lastUserId) {
        JobManager.instance().cancelAll();
        if (lastUserId != null) {
            // Schedule for today
            final LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC);
            final List<Take> todayTakeList = DatabaseService.getTakeListFor(
                    lastUserId, DayOfWeek.from(localDateTime).getValue()
            );
            scheduleFor(
                    localDateTime.getDayOfMonth(),
                    localDateTime.getMonthValue() - 1,
                    localDateTime.getYear(),
                    todayTakeList
            );
            // And look ahead one day
            final LocalDateTime tomorrow = localDateTime.plusDays(1);
            final List<Take> tomorrowTakeList = DatabaseService.getTakeListFor(
                    lastUserId, DayOfWeek.from(tomorrow).getValue()
            );
            scheduleFor(
                    tomorrow.getDayOfMonth(),
                    tomorrow.getMonthValue() - 1,
                    tomorrow.getYear(),
                    tomorrowTakeList
            );
            // Then schedule this job
            DailyJob.schedule(
                    new JobRequest.Builder(TAG),
                    TimeUnit.HOURS.toMillis(1),
                    TimeUnit.HOURS.toMillis(2)
            );
        }
    }

    private static void scheduleFor(final int day, final int month, final int year, final List<Take> takeList) {
        final Calendar calendar = Calendar.getInstance();
        Stream.of(takeList)
                .forEach(new Consumer<Take>() {
                    @Override
                    public void accept(Take take) {
                        calendar.set(year, month, day, take.getHour(), take.getMinute());
                        if (calendar.getTimeInMillis() >= System.currentTimeMillis()) {
                            final PersistableBundleCompat bundleCompat = new PersistableBundleCompat();
                            bundleCompat.putString(
                                    "message",
                                    Stream.of(take.getDrugs())
                                            .map(new Function<Drug, String>() {
                                                @Override
                                                public String apply(Drug drug) {
                                                    return drug.getName();
                                                }
                                            })
                                            .collect(Collectors.joining(", "))
                            );
                            new JobRequest
                                    .Builder(AlarmJob.TAG)
                                    .setExact(calendar.getTimeInMillis() - System.currentTimeMillis())
                                    .setExtras(bundleCompat)
                                    .build()
                                    .schedule();
                        }
                    }
                });
    }
}
