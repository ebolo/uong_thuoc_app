package com.ebolo.appuongthuoc.services.jobs;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import com.ebolo.appuongthuoc.R;
import com.evernote.android.job.Job;

import java.util.Random;

public class AlarmJob extends Job {
    final static String TAG = "alarm_job";

    @NonNull
    @Override
    protected Result onRunJob(Params params) {
        final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getContext(), TAG)
                .setContentTitle("Hey, it's time for:")
                .setSmallIcon(R.drawable.ic_menu_camera)
                .setContentText(params.getExtras().getString("message", "NULL"))
                .setShowWhen(true)
                .setLocalOnly(true);
        final NotificationManager notificationManager = (NotificationManager)
                getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                final NotificationChannel notificationChannel = notificationManager.getNotificationChannel(TAG);
                if (notificationChannel == null) {
                    notificationManager.createNotificationChannel(
                            new NotificationChannel(TAG, TAG, NotificationManager.IMPORTANCE_DEFAULT)
                    );
                }
            }
            notificationManager.notify(new Random().nextInt(), notificationBuilder.build());
            return Result.SUCCESS;
        }
        return Result.FAILURE;
    }
}
