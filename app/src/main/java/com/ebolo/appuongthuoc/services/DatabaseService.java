package com.ebolo.appuongthuoc.services;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.Predicate;
import com.ebolo.appuongthuoc.database.dtos.Take;
import com.ebolo.appuongthuoc.database.dtos.TakeBuilder;
import com.ebolo.appuongthuoc.database.entities.Drug;
import com.ebolo.appuongthuoc.database.entities.Reminder;
import com.ebolo.appuongthuoc.database.entities.Schedule;
import com.ebolo.appuongthuoc.database.entities.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Book;
import io.paperdb.Paper;

public class DatabaseService {
    public static Book database(final String name) {
        return Paper.book(name);
    }

    public static List<Take> getTakeListFor(final String userId, @Schedule.ScheduleDay final int dayOfWeek) {
        final Map<String, Take> takeMap = new HashMap<>();
        // Lấy dữ liệu list drugs từ database
        Stream.of(((User) DatabaseService.database("users").read(userId, null)).getDrugs())
                // filter lấy những drug có alarm trong ngày
                .filter(new Predicate<Drug>() {
                    @Override
                    public boolean test(Drug drug) {
                        return drug.getSchedule().isAlarmingFor(dayOfWeek);
                    }
                })
                // Với mỗi drug
                .forEach(new Consumer<Drug>() {
                    @Override
                    public void accept(final Drug drug) {
                        Stream.of(drug.getReminders())
                                .forEach(new Consumer<Reminder>() {
                                    @Override
                                    public void accept(Reminder reminder) {
                                        // Kiểm tra xem đã có take tương ứng với thời gian (ví dụ như "8:00") chưa
                                        if (takeMap.get("" + reminder.getHour() + ':' + reminder.getMinute()) == null)
                                            // Chưa có thì tạo take tương ứng với thời gian
                                            takeMap.put(
                                                    "" + reminder.getHour() + ':' + reminder.getMinute(),
                                                    new TakeBuilder()
                                                            .setHour(reminder.getHour())
                                                            .setMinute(reminder.getMinute())
                                                            .createTake()
                                            );
                                        // Có rồi thì add drug đó vô list drug của take
                                        takeMap.get("" + reminder.getHour() + ':' + reminder.getMinute())
                                                .addDrug(drug);

                                    }
                                });
                    }
                });
        // Tạo list các takes trong ngày từ values của takeMap - new ArrayList<>(takeMap.values())
        // và đưa vào RecyclerView (UI component hiển thị list)
        return new ArrayList<>(takeMap.values());
    }
}
