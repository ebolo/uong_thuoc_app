package com.ebolo.appuongthuoc.utils;

public class DateTimeUtils {

    public static String stringify(final int hour, final int minute) {
        final String hourStr = (hour < 10? "0" : "") + hour;
        final String minuteStr = (minute < 10? "0" : "") + minute;
        return hourStr + ':' + minuteStr;
    }
}
