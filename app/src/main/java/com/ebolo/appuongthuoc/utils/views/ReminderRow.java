package com.ebolo.appuongthuoc.utils.views;

import android.content.Context;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import com.ebolo.appuongthuoc.R;
import com.ebolo.appuongthuoc.utils.DateTimeUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReminderRow extends FrameLayout {
    @BindView(R.id.timeButton)
    Button timeButton;
    @BindView(R.id.takeButton)
    Button takeButton;
    @BindView(R.id.deleteButton)
    ImageButton deleteButton;

    private String timeId;

    public interface OnRemoveListener { void remove(final ReminderRow reminderRow); }
    private OnRemoveListener onRemoveListener;

    public ReminderRow(Context context) {
        super(context);
        timeId = DateTimeUtils.stringify(8, 0);
        init(context);
    }

    public ReminderRow(Context context, final int hour, final int minute) {
        super(context);
        timeId = DateTimeUtils.stringify(hour, minute);
        init(context);
    }

    private void init(final Context context) {
        ButterKnife.bind(this, inflate(context, R.layout.view_reminder_row, this));
        timeButton.setText(timeId);
    }

    public String getTimeId() {
        return timeId;
    }

    @OnClick(R.id.timeButton)
    void onTimeButtonClicked() {
        Toast.makeText(getContext(), "TIME CLICKED", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.takeButton)
    void onTakeButtonClicked() {
        Toast.makeText(getContext(), "TAKE CLICKED", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.deleteButton)
    void onDeleteButtonClicked() {
        if (onRemoveListener != null)
            onRemoveListener.remove(this);
    }

    public void setOnRemoveListener(OnRemoveListener onRemoveListener) {
        this.onRemoveListener = onRemoveListener;
    }
}
