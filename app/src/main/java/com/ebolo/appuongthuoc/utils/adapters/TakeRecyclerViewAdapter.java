package com.ebolo.appuongthuoc.utils.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.ebolo.appuongthuoc.R;
import com.ebolo.appuongthuoc.database.dtos.Take;
import com.ebolo.appuongthuoc.database.entities.Drug;
import com.ebolo.appuongthuoc.fragments.TakeFragment.OnListFragmentInteractionListener;
import com.ebolo.appuongthuoc.utils.views.DrugRow;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TakeRecyclerViewAdapter extends RecyclerView.Adapter<TakeRecyclerViewAdapter.ViewHolder> {

    private final List<Take> mValues;
    private final OnListFragmentInteractionListener mListener;

    public TakeRecyclerViewAdapter(List<Take> takes, OnListFragmentInteractionListener listener) {
        mValues = takes;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_take, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.render(mValues.get(position));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final View mView;
        @BindView(R.id.timeTextView)
        TextView timeTextView;
        @BindView(R.id.skipButton)
        Button skipButton;
        @BindView(R.id.takeButton)
        Button takeButton;
        @BindView(R.id.drugsHolder)
        LinearLayout drugsHolder;

        ViewHolder(View view) {
            super(view);
            this.mView = view;
            ButterKnife.bind(this, view);
        }

        void render(final Take take) {
            final String timeString = "" + take.getHour() + ':' + take.getMinute();
            timeTextView.setText(timeString);
            Stream.of(take.getDrugs())
                    .forEach(new Consumer<Drug>() {
                        @Override
                        public void accept(Drug drug) {
                            drugsHolder.addView(new DrugRow(mView.getContext(), drug));
                        }
                    });
            this.skipButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO: Do something
                }
            });
            this.takeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO: Do something
                }
            });
        }
    }
}
