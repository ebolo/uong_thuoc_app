package com.ebolo.appuongthuoc.utils.views;

import android.content.Context;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ebolo.appuongthuoc.R;
import com.ebolo.appuongthuoc.database.entities.Drug;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DrugRow extends FrameLayout {
    @BindView(R.id.drugName)
    TextView drugName;

    private Drug drug;

    public DrugRow(Context context) {
        super(context);
        init(context);
    }

    public DrugRow(Context context, final Drug drug) {
        super(context);
        this.drug = drug;
        init(context);
    }

    private void init(final Context context) {
        ButterKnife.bind(this, inflate(context, R.layout.view_drug_row, this));
        drugName.setText(drug != null? drug.getName() : "");
    }
}
