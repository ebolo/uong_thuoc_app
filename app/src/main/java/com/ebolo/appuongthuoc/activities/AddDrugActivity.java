package com.ebolo.appuongthuoc.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import com.ebolo.appuongthuoc.R;
import com.ebolo.appuongthuoc.database.entities.Drug;
import com.ebolo.appuongthuoc.database.entities.DrugBuilder;
import com.ebolo.appuongthuoc.database.entities.Instruction;
import com.ebolo.appuongthuoc.database.entities.Reminder;
import com.ebolo.appuongthuoc.database.entities.Schedule;
import com.ebolo.appuongthuoc.database.entities.User;
import com.ebolo.appuongthuoc.fragments.AddReminderFragment;
import com.ebolo.appuongthuoc.services.DatabaseService;
import com.ebolo.appuongthuoc.services.PreferenceService;
import com.ebolo.appuongthuoc.services.UiService;
import com.ebolo.appuongthuoc.services.jobs.AlarmUpdateJob;
import com.ebolo.appuongthuoc.utils.DateTimeUtils;
import com.ebolo.appuongthuoc.utils.views.ReminderRow;
import com.google.common.primitives.Ints;

import org.threeten.bp.Instant;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneOffset;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ebolo.appuongthuoc.database.entities.Instruction.AFTER;
import static com.ebolo.appuongthuoc.database.entities.Instruction.BEFORE;
import static com.ebolo.appuongthuoc.database.entities.Instruction.WITH;

public class AddDrugActivity extends AppCompatActivity
        implements  ReminderRow.OnRemoveListener,
                    AddReminderFragment.OnSetListener,
                    DatePickerDialog.OnDateSetListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drugName)
    TextView drugName;

    @BindView(R.id.reminderAddButton)
    ImageButton reminderAddButton;
    @BindView(R.id.remindersHolder)
    LinearLayout remindersHolder;

    @BindView(R.id.dateButton)
    Button dateButton;
    @BindView(R.id.mondayCheckBox)
    CheckBox mondayCheckBox;
    @BindView(R.id.tuesdayCheckBox)
    CheckBox tuesdayCheckBox;
    @BindView(R.id.wednesdayCheckBox)
    CheckBox wednesdayCheckBox;
    @BindView(R.id.thursdayCheckBox)
    CheckBox thursdayCheckBox;
    @BindView(R.id.fridayCheckBox)
    CheckBox fridayCheckBox;
    @BindView(R.id.saturdayCheckBox)
    CheckBox saturdayCheckBox;
    @BindView(R.id.sundayCheckBox)
    CheckBox sundayCheckBox;

    @BindView(R.id.freeInstruction)
    EditText freeInstruction;
    @BindView(R.id.foodInstruction)
    RadioGroup foodInstruction;

    private DrugBuilder drugBuilder;
    private Schedule schedule;
    private Instruction instruction;
    private List<Reminder> reminderList;
    private Map<String, Float> reminderInfoMap;

    private SimpleDateFormat dateFormat = new SimpleDateFormat(
            "MMM dd, yyyy", Locale.getDefault()
    );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_drug);
        ButterKnife.bind(this);

        final boolean returnedFromPrevious = savedInstanceState != null
                && savedInstanceState.getSerializable("temporaryDrug") != null;
        // Nếu dữ liệu (instruction, schedule, drugbuilder) đã được lưu trước đó thì lấy lại
        // từng cái, không thì tạo mới
        if (returnedFromPrevious) {
            final Drug temporaryDrug = (Drug) savedInstanceState.getSerializable("temporaryDrug");
            assert temporaryDrug != null;
            drugName.setText(temporaryDrug.getName());
            schedule = temporaryDrug.getSchedule();
            instruction = temporaryDrug.getInstruction();
            reminderList = temporaryDrug.getReminders();
        }
        drugBuilder = new DrugBuilder();

        if (schedule != null) {
            mondayCheckBox.setChecked(schedule.isAlarmingFor(Schedule.MONDAY));
            tuesdayCheckBox.setChecked(schedule.isAlarmingFor(Schedule.TUESDAY));
            wednesdayCheckBox.setChecked(schedule.isAlarmingFor(Schedule.WEDNESDAY));
            thursdayCheckBox.setChecked(schedule.isAlarmingFor(Schedule.THURSDAY));
            fridayCheckBox.setChecked(schedule.isAlarmingFor(Schedule.FRIDAY));
            saturdayCheckBox.setChecked(schedule.isAlarmingFor(Schedule.SATURDAY));
            sundayCheckBox.setChecked(schedule.isAlarmingFor(Schedule.SUNDAY));

        } else schedule = new Schedule();

        dateButton.setText(
                dateFormat.format(org.threeten.bp.DateTimeUtils.toDate(schedule.getStartDate()))
        );

        if (instruction == null) instruction = new Instruction();
        // Check vào ô tương ứng với food instruction hiện tại
        switch (instruction.getFoodInstruction()) {
            case BEFORE:
                foodInstruction.check(R.id.beforeButton);
                break;
            case WITH:
                foodInstruction.check(R.id.withButton);
                break;
            case AFTER:
                foodInstruction.check(R.id.afterButton);
                break;
        }
        // Khi food instruction thay đổi thì data cũng thay đổi theo
        foodInstruction.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.beforeButton:
                        instruction.setFoodInstruction(BEFORE);
                        break;
                    case R.id.withButton:
                        instruction.setFoodInstruction(WITH);
                        break;
                    case R.id.afterButton:
                        instruction.setFoodInstruction(AFTER);
                        break;
                }
            }
        });
        freeInstruction.setText(instruction.getFreeInstruction());

        reminderInfoMap = new HashMap<>();
        if (reminderList != null) {
            for (Reminder reminder : reminderList) {
                final ReminderRow reminderRow = new ReminderRow(
                        this, reminder.getHour(), reminder.getMinute()
                );
                reminderRow.setOnRemoveListener(this);
                remindersHolder.addView(reminderRow);
                reminderInfoMap.put(
                        DateTimeUtils.stringify(reminder.getHour(), reminder.getMinute()),
                        reminder.getQuantity()
                );
            }
        } else reminderList = new ArrayList<>();

        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Set ngày cho nút chọn ngày (mặc định là ngày hôm nay)
        dateButton.setText(dateFormat.format(new Date()));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        instruction.setFreeInstruction(freeInstruction.getText().toString());
        final Drug temporaryDrug = new Drug(
                drugName.getText().toString(),
                reminderList,
                instruction,
                schedule
        );
        outState.putSerializable("temporaryDrug", temporaryDrug);
    }

    @OnClick(R.id.dateButton)
    void onDateButtonClicked() {
        UiService.showDatePicker(
                LocalDateTime.ofInstant(schedule.getStartDate(), ZoneOffset.UTC),
                this, getSupportFragmentManager()
        );
    }

    @OnClick(R.id.reminderAddButton)
    void onReminderAddButtonClicked() {
        final DialogFragment fragment = AddReminderFragment.newInstance(this);
        fragment.show(getSupportFragmentManager(), AddReminderFragment.class.getSimpleName());
    }

    @OnClick(R.id.mondayCheckBox)
    void onMondayChecked() {
        schedule.setDate(Schedule.MONDAY, mondayCheckBox.isChecked());
    }

    @OnClick(R.id.tuesdayCheckBox)
    void onTuesdayChecked() {
        schedule.setDate(Schedule.TUESDAY, tuesdayCheckBox.isChecked());
    }

    @OnClick(R.id.wednesdayCheckBox)
    void onWednesdayChecked() {
        schedule.setDate(Schedule.WEDNESDAY, wednesdayCheckBox.isChecked());
    }

    @OnClick(R.id.thursdayCheckBox)
    void onThursdayChecked() {
        schedule.setDate(Schedule.THURSDAY, thursdayCheckBox.isChecked());
    }

    @OnClick(R.id.fridayCheckBox)
    void onFridayChecked() {
        schedule.setDate(Schedule.FRIDAY, fridayCheckBox.isChecked());
    }

    @OnClick(R.id.saturdayCheckBox)
    void onSaturdayChecked() {
        schedule.setDate(Schedule.SATURDAY, saturdayCheckBox.isChecked());
    }

    @OnClick(R.id.sundayCheckBox)
    void onSundayChecked() {
        schedule.setDate(Schedule.SUNDAY, sundayCheckBox.isChecked());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_drug, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.doneButton:
                if (drugName.getText().toString().trim().isEmpty()) {
                    drugName.setError("Not empty!");
                    break;
                }
                instruction.setFreeInstruction(freeInstruction.getText().toString());
                drugBuilder
                        .setName(drugName.getText().toString())
                        .setSchedule(schedule)
                        .setReminders(reminderList)
                        .setInstruction(instruction);
                final String userId = PreferenceService.getPreference(
                        this, getString(R.string.general_pref_file_key)
                ).getString("lastUserId", null);
                final User user = DatabaseService.database("users").read(userId);
                user.getDrugs().add(drugBuilder.createDrug());
                DatabaseService.database("users").write(userId, user);
                AlarmUpdateJob.reSchedule(userId);
                setResult(Activity.RESULT_OK);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    // Hàm của OnRemoveListener của reminder row
    @Override
    public void remove(ReminderRow reminderRow) {
        final String reminderId = reminderRow.getTimeId();
        reminderInfoMap.put(reminderId, null);
        final Reminder reminder = Stream.of(reminderList)
                .filter(new Predicate<Reminder>() {
                    @Override
                    public boolean test(Reminder value) {
                        return DateTimeUtils
                                .stringify(value.getHour(), value.getMinute())
                                .equals(reminderId);
                    }
                })
                .single();
        reminderList.remove(reminder);
        remindersHolder.removeView(reminderRow);
    }

    // Hàm của OnSetListener của Add reminder fragment
    @Override
    public void set(final int hour, final int minute, float take) {
        if (reminderInfoMap.get(DateTimeUtils.stringify(hour, minute)) != null) {
            new AlertDialog.Builder(this)
                    .setMessage("There has already been a reminder withing setting time!")
                    .create()
                    .show();
        } else {
            final ReminderRow reminderRow = new ReminderRow(this, hour, minute);
            reminderRow.setOnRemoveListener(this);
            reminderInfoMap.put(DateTimeUtils.stringify(hour, minute), take);
            int pos = reminderList.size() - Ints.checkedCast(
                    Stream.of(reminderList)
                            .filter(new Predicate<Reminder>() {
                                @Override
                                public boolean test(Reminder value) {
                                    return value.getHour() > hour
                                            || (value.getHour() == hour && value.getMinute() > minute);
                                }
                            })
                            .count()
            );
            reminderList.add(pos, new Reminder(hour, minute, take));
            remindersHolder.addView(reminderRow, pos);
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dayOfMonth);
        schedule.setStartDate(Instant.ofEpochMilli(calendar.getTimeInMillis()));
        dateButton.setText(dateFormat.format(calendar.getTime()));
    }
}
