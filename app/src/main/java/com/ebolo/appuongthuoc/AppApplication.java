package com.ebolo.appuongthuoc;

import android.app.Application;
import android.content.SharedPreferences;

import com.ebolo.appuongthuoc.database.entities.User;
import com.ebolo.appuongthuoc.services.jobs.JobsCreator;
import com.evernote.android.job.JobManager;

import io.paperdb.Paper;

public class AppApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // Initialize database
        Paper.init(this);
        // Initialize job manager
        JobManager.create(this).addJobCreator(new JobsCreator());

        // TODO: This is only applicable in testing phase
        prepareData();
    }

    private void prepareData() {
        SharedPreferences generalPrefs = this.getSharedPreferences(
                getString(R.string.general_pref_file_key), MODE_PRIVATE
        );
        String lastUserId = generalPrefs.getString("lastUserId", null);
        if (lastUserId == null) {
            final User user = new User("Ebolo");
            Paper.book("users").write(user.getId(), user);
            generalPrefs
                    .edit()
                    .putString("lastUserId", user.getId())
                    .apply();
        }
    }
}
