package com.ebolo.appuongthuoc.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.TextStyle;

import java.util.Locale;

public class DatePickerFragment extends DialogFragment {
    private LocalDateTime localDateTime;
    private DatePickerDialog.OnDateSetListener listener;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DatePickerFragment() {}

    public static DatePickerFragment newInstance(
            final DatePickerDialog.OnDateSetListener listener,
            final LocalDateTime localDateTime
    ) {
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setListener(listener);
        fragment.setLocalDateTime(localDateTime);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        int year = localDateTime.getYear();
        int month = localDateTime.getMonthValue() - 1;
        int day = localDateTime.getDayOfMonth();
        localDateTime.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.getDefault());

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), listener, year, month, day);

    }

    public void setListener(DatePickerDialog.OnDateSetListener listener) {
        this.listener = listener;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }
}
