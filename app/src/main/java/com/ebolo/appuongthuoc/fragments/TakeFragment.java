package com.ebolo.appuongthuoc.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;

import com.ebolo.appuongthuoc.R;
import com.ebolo.appuongthuoc.activities.AddDrugActivity;
import com.ebolo.appuongthuoc.database.dtos.Take;
import com.ebolo.appuongthuoc.database.entities.User;
import com.ebolo.appuongthuoc.services.DatabaseService;
import com.ebolo.appuongthuoc.services.PreferenceService;
import com.ebolo.appuongthuoc.services.UiService;
import com.ebolo.appuongthuoc.utils.RequestCodes;
import com.ebolo.appuongthuoc.utils.adapters.TakeRecyclerViewAdapter;

import org.threeten.bp.DayOfWeek;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneOffset;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class TakeFragment extends Fragment implements DatePickerDialog.OnDateSetListener {
    @BindView(R.id.dateButton)
    Button dateButton;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.takeList)
    RecyclerView takeRecyclerView;

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    private List<Take> takeList;
    private Instant currentDisplayingInstant;
    private LocalDateTime localDateTime;
    private String lastUserId;

    private SimpleDateFormat format =
            new SimpleDateFormat("EEEE, dd/MM/yyyy", Locale.getDefault());

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TakeFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static TakeFragment newInstance(int columnCount) {
        TakeFragment fragment = new TakeFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Nếu là back từ home thì lấy giờ đang hiển thị từ bundle đã lưu
        if (savedInstanceState != null) {
            final String savedInstantStr = savedInstanceState
                    .getString("currentDisplayingInstant");
            if (savedInstantStr != null)
                currentDisplayingInstant = Instant.parse(savedInstantStr);
        }
        // Còn không thì lấy giờ hiện tại
        if (currentDisplayingInstant == null)
            currentDisplayingInstant = Instant.now();
        localDateTime = LocalDateTime.ofInstant(currentDisplayingInstant, ZoneOffset.UTC);

        // Lấy userId của user ở lần hiển thị mới nhất trước đó
        lastUserId = PreferenceService.getPreference(
                getActivity(), getString(R.string.general_pref_file_key)
        ).getString("lastUserId", null);
        // Đặt toolbar hiển thị tên user
        getActivity().setTitle(
                ((User) DatabaseService.database("users").read(lastUserId)).getName()
        );
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_take_list, container, false);
        ButterKnife.bind(this, view);

        // Set the adapter
        Context context = view.getContext();
        if (mColumnCount <= 1) {
            takeRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            takeRecyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
        dateButton.setText(format.format(new Date()));
        refreshTakeList();
        return view;
    }

    @OnClick(R.id.dateButton)
    void chooseDate() {
        UiService.showDatePicker(localDateTime, this, getFragmentManager());
    }

    @OnClick(R.id.fab)
    void onFabClicked() {
        final Intent addDrugIntent = new Intent(getContext(), AddDrugActivity.class);
        startActivityForResult(addDrugIntent, RequestCodes.ADD_DRUG_REQUEST);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch(requestCode) {
                case RequestCodes.ADD_DRUG_REQUEST:
                    refreshTakeList();
                    break;
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("currentDisplayingInstant", localDateTime.toInstant(ZoneOffset.UTC)
                .toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dayOfMonth);
        dateButton.setText(format.format(calendar.getTime()));
        localDateTime = LocalDateTime.of(year, month + 1, dayOfMonth, 8, 0);
        refreshTakeList();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Take item);
    }

    private void  refreshTakeList() {
        // Lấy take list cho thứ trong tuần (của ngày) rồi đưa vào recycler view
        // (ui component hiển thị list)
        takeList = DatabaseService
                .getTakeListFor(lastUserId, DayOfWeek.from(localDateTime).getValue());
        takeRecyclerView.setAdapter(new TakeRecyclerViewAdapter(takeList, mListener));
    }
}
