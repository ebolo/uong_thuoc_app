package com.ebolo.appuongthuoc.fragments;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TimePicker;

import com.ebolo.appuongthuoc.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.polak.clicknumberpicker.ClickNumberPickerView;

public class AddReminderFragment extends DialogFragment {
    @BindView(R.id.timePicker)
    TimePicker timePicker;
    @BindView(R.id.numberPicker)
    ClickNumberPickerView numberPicker;
    @BindView(R.id.setButton)
    Button setButton;

    public interface OnSetListener{
        void set(final int hour, final int minute, final float take);
    }
    private OnSetListener onSetListener;

    public AddReminderFragment() {}

    public static AddReminderFragment newInstance(final OnSetListener onSetListener) {
        final AddReminderFragment fragment = new AddReminderFragment();
        fragment.setOnSetListener(onSetListener);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_add_reminder, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick(R.id.setButton)
    void onSetButtonClicked() {
        if (onSetListener != null) {
            onSetListener.set(
                    timePicker.getCurrentHour(),
                    timePicker.getCurrentMinute(),
                    numberPicker.getValue()
            );
        }
        dismiss();
    }

    public void setOnSetListener(OnSetListener onSetListener) {
        this.onSetListener = onSetListener;
    }
}
